package org.vaultage.shared.fs;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

import org.vaultage.shared.fs.meta.MetaFile;

public class FileData implements Serializable {

  private static final long serialVersionUID = 201302051242L;

  private final MetaFile metaFile;
  private final byte[] data;

  FileData(MetaFile metaFile, byte[] data) {
    this.metaFile = Objects.requireNonNull(metaFile);
    this.data = Objects.requireNonNull(data);
  }

  public MetaFile getMetaFile() {
    return metaFile;
  }

  public byte[] getData() {
    return Arrays.copyOf(data, data.length);
  }

  @Override
  public int hashCode() {
    return Objects.hash(metaFile.hashCode(), Arrays.hashCode(data));
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof FileData) {
      FileData other = (FileData) obj;
      return Objects.equals(metaFile, other.metaFile) && Arrays.equals(data, other.data);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "FileData [path=" + metaFile.getPath() + ", changeDate=" + metaFile.getChangeDate()
        + ", hash=" + metaFile.getHash() + "]";
  }

}
