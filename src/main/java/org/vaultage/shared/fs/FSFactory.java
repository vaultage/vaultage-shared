package org.vaultage.shared.fs;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaultage.shared.comm.auth.CodecUtils;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;
import org.vaultage.shared.fs.meta.MetaFolder;

public final class FSFactory {

  private static final Logger LOGGER = LoggerFactory.getLogger(FSFactory.class);

  public static final IMeta getMeta(Path path) throws IOException {
    return getMeta(path, null);
  }

  public static final IMeta getMeta(Path path, Path relPath) throws IOException {
    LOGGER.trace("Getting meta for path '" + path + "'");
    Date changeDate = new Date(Files.getLastModifiedTime(path).toMillis());
    String pathStr = (relPath != null ? relPath : path).toString();
    if (Files.isDirectory(path)) {
      return new MetaFolder(pathStr, changeDate);
    } else {
      return new MetaFile(pathStr, changeDate, generateHash(path));
    }
  }

  static String generateHash(Path path) throws IOException {
    FileInputStream fis = null;
    try {
      fis = new FileInputStream(path.toFile());
      return CodecUtils.sha1(fis);
    } finally {
      IOUtils.closeQuietly(fis);
    }
  }

  public static final FileData getData(Path path, MetaFile metaFile) throws IOException {
    byte[] data = Files.readAllBytes(path.resolve(metaFile.getPath()));
    if (metaFile.getHash().equals(CodecUtils.sha1(data))) {
      return new FileData(metaFile, data);
    } else {
      throw new IOException("Hash comparison failed for '" + metaFile + "'");
    }
  }

}
