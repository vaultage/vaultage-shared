package org.vaultage.shared.fs.meta;

import java.util.Date;
import java.util.Objects;

public class MetaFolder implements IMeta {

  private static final long serialVersionUID = 201302051230L;

  private final String path;
  private final Date changeDate;

  public MetaFolder(String path, Date changeDate) {
    this.path = Objects.requireNonNull(path);
    this.changeDate = changeDate;
  }

  @Override
  public final String getPath() {
    return path;
  }

  @Override
  public final Date getChangeDate() {
    return changeDate;
  }

  @Override
  public String getHash() {
    return null;
  }

  @Override
  public boolean isFolder() {
    return !(this instanceof MetaFile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, changeDate);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof MetaFolder) {
      MetaFolder other = (MetaFolder) obj;
      return Objects.equals(path, other.path) && Objects.equals(changeDate, other.changeDate);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "MetaFolder [path=" + path + ", changeDate=" + changeDate + "]";
  }

}
