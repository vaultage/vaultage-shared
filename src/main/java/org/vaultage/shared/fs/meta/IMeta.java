package org.vaultage.shared.fs.meta;

import java.io.Serializable;
import java.util.Date;

public interface IMeta extends Serializable {

  public String getPath();

  public Date getChangeDate();

  public String getHash();

  public boolean isFolder();

}
