package org.vaultage.shared.fs;

public class FSObjectBuildException extends Exception {

  private static final long serialVersionUID = 201312261544L;

  public FSObjectBuildException() {
    super();
  }

  public FSObjectBuildException(String msg) {
    super(msg);
  }

  public FSObjectBuildException(String msg, Throwable cause) {
    super(msg, cause);
  }

}
