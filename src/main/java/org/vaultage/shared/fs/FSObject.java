package org.vaultage.shared.fs;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class FSObject<E> implements Serializable {

  private static final long serialVersionUID = 201312171323L;

  private final String root;
  private final Map<String, E> elementMap;

  public FSObject(String root) {
    this.root = Objects.requireNonNull(root);
    elementMap = new HashMap<>();
  }

  public String getRoot() {
    return root;
  }

  public E get(String key) {
    return elementMap.get(key);
  }

  public Map<String, E> getClonedMap() {
    return new HashMap<>(elementMap);
  }

  public Map<String, E> getUnmodifiableMap() {
    return Collections.unmodifiableMap(elementMap);
  }

  public boolean put(String key, E element) {
    if ((element != null) && !elementMap.containsKey(element)) {
      elementMap.put(key, element);
      return true;
    }
    return false;
  }

  public int size() {
    return elementMap.size();
  }

  @Override
  public String toString() {
    return "FSObject [root=" + root + ", elementMap=" + elementMap + "]";
  }

}
