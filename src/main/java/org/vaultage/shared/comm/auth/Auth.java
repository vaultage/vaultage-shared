package org.vaultage.shared.comm.auth;

import java.io.Serializable;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public final class Auth implements Serializable {

  private static final long serialVersionUID = 201302010213L;

  private final String hash;

  public Auth(String password, String salt, String challenge) throws InvalidKeySpecException {
    this.hash = CodecUtils.generateSaltedChallengeHash(Objects.requireNonNull(password),
        Objects.requireNonNull(salt), Objects.requireNonNull(challenge));
  }

  public String getHash() {
    return hash;
  }

  @Override
  public String toString() {
    return "Auth [hash=" + hash + "]";
  }

}
