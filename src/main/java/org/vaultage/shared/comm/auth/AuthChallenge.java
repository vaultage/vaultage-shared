package org.vaultage.shared.comm.auth;

import java.io.Serializable;
import java.util.Objects;

public final class AuthChallenge implements Serializable {

  private static final long serialVersionUID = 201311210242L;

  private final String salt;
  private final String challenge;

  public AuthChallenge(String salt, String challenge) {
    this.salt = Objects.requireNonNull(salt);
    this.challenge = Objects.requireNonNull(challenge);
  }

  public String getSalt() {
    return salt;
  }

  public String getChallenge() {
    return challenge;
  }

  @Override
  public String toString() {
    return "AuthChallenge [salt=" + salt + ", challenge=" + challenge + "]";
  }

}
