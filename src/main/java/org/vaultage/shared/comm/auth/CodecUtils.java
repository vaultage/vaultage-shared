package org.vaultage.shared.comm.auth;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class CodecUtils {

  private static SecretKeyFactory keyFactory;

  public static final String generateSaltedChallengeHash(String password, String salt,
      String challenge) throws InvalidKeySpecException {
    return generateChallengeHash(generateSaltedHash(password, salt), challenge);
  }

  public static final String generateSaltedHash(String password, String salt)
      throws InvalidKeySpecException {
    KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(Charset.forName("UTF-8")),
        65536, 128);
    return DigestUtils.sha512Hex(getKeyFactory().generateSecret(spec).getEncoded());
  }

  public static final String generateChallengeHash(String hash, String challenge) {
    return DigestUtils.sha512Hex(hash + challenge);
  }

  public static final String generateFakedSalt(String secret, String name) {
    return DigestUtils.sha512Hex(secret + name);
  }

  public static final String sha1(byte[] data) {
    return DigestUtils.sha1Hex(data);
  }

  public static final String sha1(InputStream is) throws IOException {
    return DigestUtils.sha1Hex(is);
  }

  public static final String generateRandomString(int size) {
    return RandomStringUtils.random(size, 0, 0, true, true, null, new SecureRandom());
  }

  private static SecretKeyFactory getKeyFactory() {
    if (keyFactory == null) {
      try {
        keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
      } catch (NoSuchAlgorithmException exc) {
        // silent catch
      }
    }
    return keyFactory;
  }

}
