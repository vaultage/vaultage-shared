package org.vaultage.shared.comm.auth;

import java.io.Serializable;
import java.util.Objects;

public final class AuthRequest implements Serializable {

  private static final long serialVersionUID = 201311210240L;

  private final String username;

  public AuthRequest(String username) {
    this.username = Objects.requireNonNull(username);
  }

  public String getUsername() {
    return username;
  }

  @Override
  public String toString() {
    return "AuthRequest [username=" + username + "]";
  }

}
