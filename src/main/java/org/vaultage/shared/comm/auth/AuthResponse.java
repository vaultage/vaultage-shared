package org.vaultage.shared.comm.auth;

public enum AuthResponse {

  SUCCESS, FAILURE;
  
  @Override
  public String toString() {
	  return "AuthResponse [" + super.toString() + "]";
  }

}
