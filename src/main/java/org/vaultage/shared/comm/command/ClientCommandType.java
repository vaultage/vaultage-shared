package org.vaultage.shared.comm.command;

import org.vaultage.shared.comm.auth.AuthChallenge;
import org.vaultage.shared.comm.auth.AuthResponse;
import org.vaultage.shared.fs.meta.MetaFile;

public enum ClientCommandType implements ICommandType {

  ErrorCommand(String.class),
  AuthChallengeCommand(AuthChallenge.class),
  AuthResponseCommand(AuthResponse.class),
  SyncStartCommand(Boolean.class),
  FileRequestCommand(MetaFile.class),
  FileResponseCommand(Boolean.class),
  SyncEndCommand(Boolean.class),
  TestCommand(Object.class);

  private Class<?> clazz;

  private ClientCommandType(Class<?> clazz) {
    this.clazz = clazz;
  }

  @Override
  public Class<?> getDataClass() {
    return clazz;
  }

}
