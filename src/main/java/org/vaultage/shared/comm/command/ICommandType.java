package org.vaultage.shared.comm.command;

public interface ICommandType {

  public String name();

  public Class<?> getDataClass();

}
