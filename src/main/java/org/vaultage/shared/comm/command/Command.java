package org.vaultage.shared.comm.command;

import java.io.Serializable;
import java.util.Objects;

public class Command<T> implements ICommand<T>, Serializable {

  private static final long serialVersionUID = 201309190200L;

  private final ICommandType type;
  private final String sender;
  private final String receiver;
  private boolean isValid;
  protected final T data;

  Command(ICommandType type, String sender, String receiver, T data) {
    this.type = Objects.requireNonNull(type);
    this.sender = Objects.requireNonNull(sender);
    this.receiver = Objects.requireNonNull(receiver);
    this.data = Objects.requireNonNull(data);
    this.isValid = false;
    if (!type.getDataClass().equals(data.getClass())) {
      throw new IllegalArgumentException("Illegal data class '" + data.getClass() + "' for type '"
          + type + "'");
    }
  }

  @Override
  public ICommandType getType() {
    return type;
  }

  @Override
  public String getSender() {
    return sender;
  }

  @Override
  public String getReceiver() {
    return receiver;
  }

  @Override
  public T getData() {
    return data;
  }

  @Override
  public boolean isValid(String sender, String receiver) {
    return isValid = (!isValid && this.sender.equals(sender) && this.receiver.equals(receiver));
  }

  @Override
  public String toString() {
    return this.type.name() + " [sender=" + sender + ", receiver=" + receiver + ", isValid="
        + isValid + ", data=" + data + "]";
  }

}
