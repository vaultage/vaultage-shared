package org.vaultage.shared.comm.command;

import org.vaultage.shared.comm.auth.Auth;
import org.vaultage.shared.comm.auth.AuthRequest;
import org.vaultage.shared.fs.FSObject;
import org.vaultage.shared.fs.FileData;

public enum ServerCommandType implements ICommandType {

  AuthRequestCommand(AuthRequest.class),
  AuthCommand(Auth.class),
  SyncRequestCommand(FSObject.class),
  FileSendCommand(FileData.class),
  SyncStopCommand(String.class),
  TestCommand(Object.class);

  private Class<?> clazz;

  private ServerCommandType(Class<?> clazz) {
    this.clazz = clazz;
  }

  @Override
  public Class<?> getDataClass() {
    return clazz;
  }

}
