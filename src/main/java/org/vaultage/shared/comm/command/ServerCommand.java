package org.vaultage.shared.comm.command;

public class ServerCommand<T> extends Command<T> {

  private static final long serialVersionUID = 201312162230L;

  public ServerCommand(ServerCommandType type, String sender, String receiver, T data) {
    super(type, sender, receiver, data);
  }

  @Override
  public ServerCommandType getType() {
    return (ServerCommandType) super.getType();
  }

}
