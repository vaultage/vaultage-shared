package org.vaultage.shared.comm.command;

public class ClientCommand<T> extends Command<T> {

  private static final long serialVersionUID = 201312162226L;

  public ClientCommand(ClientCommandType type, String sender, String receiver, T data) {
    super(type, sender, receiver, data);
  }

  @Override
  public ClientCommandType getType() {
    return (ClientCommandType) super.getType();
  }

}
