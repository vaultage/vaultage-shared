package org.vaultage.shared.comm.command;

public interface ICommand<T> {

  public ICommandType getType();

  public String getSender();

  public String getReceiver();

  public T getData();

  public boolean isValid(String sender, String receiver);

}
