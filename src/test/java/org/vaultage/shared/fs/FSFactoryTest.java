package org.vaultage.shared.fs;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.vaultage.shared.fs.meta.IMeta;
import org.vaultage.shared.fs.meta.MetaFile;

import ch.marcsladek.commons.io.fs.FileCreator;

public class FSFactoryTest {

  private final Path dir = Paths.get("test");
  private final Path file = dir.resolve("asdf");
  private final String content = "muhu ;)";

  @Before
  public void setUp() throws IOException {
    FileCreator.createFile(dir, file.getFileName().toString(), content.getBytes());
  }

  @Test
  public void testGenerateHash() throws Exception {
    IMeta metaData = FSFactory.getMeta(file);
    String hash = DigestUtils.sha1Hex(content.getBytes());
    assertEquals(hash, ((MetaFile) metaData).getHash());
  }

  @After
  public void breakDown() throws IOException {
    Files.delete(file);
    Files.delete(dir);
  }

}
